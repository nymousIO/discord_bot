require 'discordrb'
require 'configatron'
require_relative '../config.rb'

# @type bot [Discordrb::Bot]
bot = Discordrb::Bot.new token: configatron.token,
                         client_id: configatron.client_id

puts "This bot's invite URL is #{bot.invite_url}."
puts 'Click on it to invite it to your server.'

bot.run :async
# @type channel [Discordrb::Channel]
channel = bot.channel(243440023234543617)

# @type messages [Array<Discordrb::Message>]
messages = channel.history 100

p "This will prune #{messages.count} messages from channel '#{channel.name}'"
p "Last message was '#{messages.first.content}' sent by #{messages.first.author.display_name}" unless messages.empty?
input = 0
while input != 'N' && input != 'Y'
  p 'Do you really want to delete? [y/n]'
  input = gets.chomp.to_s.capitalize
  case input
    when 'Y'
      if messages.count >= 2
        channel.prune messages.count
      else
        messages.first.delete
      end
    when 'N'
      exit
  end
end

# Not necessary anymore
# bot.sync
