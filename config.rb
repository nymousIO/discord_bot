configatron.token = ENV['CRBOT_TOKEN']
configatron.client_id = ENV['CRBOT_ID'].to_i
configatron.version = '1.1.0b'
configatron.prefix = '.'

configatron.sound_extensions = %w(.mp3 .wma .ogg .wav)
