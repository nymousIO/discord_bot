CRBot
=====

Bot intended for use on Rezoleo/CAG Discord server.

It is developed in Ruby, using [discordrb](https://github.com/meew0/discordrb)
library by [Meew0](https://github.com/meew0).

Modular structure comes from [sapphire bot](https://github.com/Cyan101/sapphire/)
by [Cyan101](https://github.com/Cyan101).

## Features
  - `.help`: displays help for all commands
  - Vote commands:
    - `.vote`: start a vote using reactions
  - Voice commands:
    - `.sounds`: lists all sounds that can be used in a voice channel
    - `.sound <soundname>`: plays a sound in the voice channel you're in (alias `.s`)
    - `.stop`: stop a playing sound
    - `addsound`: adds the uploaded file as a sound (used as a comment on an upload)
    - `.removesound <soundname>`: deletes the matching sound
  - Basic commands
    - `.ping`: replies "Pong", useful to check if the bot is still alive
    - `.uptime`: replies how long the bot has been running
    - `.version`: replies the current version of the bot
    - `.online`: gives the amount of active, inactive and bot users currently connected
  - Admin commands
    - `.clean <n>`: deletes the last *n* messages (n < 99) (alias: `.prune`)
  - Control commands
    - `.setusername <name>`: changes the **username** of the bot (not the nickname!)
    - `.setavatar <URL | file>`: changes the avatar of the bot (takes a url or an attachment)
    - `.exit`: well... shuts down the bot I guess (alias: `.shutdown`, `.quit`)
    - `.eval <code>`: runs ruby code directly ⚠ CAN BE DANGEROUS ⚠

## TODO
 - Secret command ;)
 - Super secret command :p
 - Pizza command
 - Opened local command
 - Rez role command
 - Local keys owner command
 - Set nickname of the bot
 - NOT RUN AS ROOT

## Deploy
1. Install rbenv, rbenv-install and ruby 2.4.1
2. Clone the repo
3. `gem install bundler`
4. `bundle install`
5. Create a `.env` file in root folder, with the following content:
```
CRBOT_ENVIRONMENT=production
CRBOT_TOKEN=token-here
CRBOT_ID=bot-id-here
```
(or populate environment variables, e.g. on Heroku)
6. Set `CRBOT_ENVIRONMENT` to either `development` or `production`
7. `ruby crbot.rb`

If you want to daemonize the bot, a Procfile is integrated. Is is used by Heroku as well.
We also use Foreman to help with the creation of service files for systemd.

1. `gem install foreman`
3. `foreman export systemd /lib/systemd/system -u root -a crbot`
4. `service crbot-crbot@5000 start` (use completion for service name)

## Contributing
If you want to contribute, make sure you set the file encoding to UTF8.

Another important thing is to triple-check when you insert a Unicode emoji, to
make sure you only have one character (you can go from before to after the
character by pressing arrow keys only once). It happened several times that by
copy/pasting from a website, it added an invisible zero-width character after
the emoji, which broke discord...

## Author
Thomas 'Nymous' Gaudin

## License
CRBot is released under the [MIT License](http://www.opensource.org/licenses/MIT).
