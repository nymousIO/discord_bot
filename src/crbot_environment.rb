class CrbotEnvironment
  include Singleton

  attr_accessor :environment

  def initialize
    @environment = ENV['CRBOT_ENVIRONMENT']
  end

  def dev?
    @environment == 'development'
  end

  def prod?
    @environment == 'production'
  end
end
