require 'open-uri'

class String
  def without_ext
    File.basename(self, File.extname(self))
  end
end

module Bot
  module DiscordCommands
    module VoiceCommands
      extend Discordrb::Commands::CommandContainer

      SOUNDS_PATH = File.absolute_path(File.join('files', 'sounds'))

      # @type ACCEPTED_EXTENSIONS [Array]
      ACCEPTED_EXTENSIONS = configatron.sound_extensions

      ADD_SOUND_DESCRIPTION = "Ajoute un son au bot. Utilisez cette commande en commentaire d'un fichier uploadé"
      LIST_SOUNDS_DESCRIPTION = 'Liste tous les sons ajoutés'
      REMOVE_SOUND_DESCRIPTION = 'Supprime un son'
      PLAY_SOUND_DESCRIPTION = 'Lit un son dans ton chan vocal'
      STOP_SOUND_DESCRIPTION = 'Arrête de lire un son'

      command(:addsound, description: ADD_SOUND_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        # @type [Discordrb::Message]
        m = event.message

        if m.attachments.empty?
          m.delete
          event.send_temp 'Uploade un fichier pour utiliser la commande !', 2*TEMP_MESSAGE_DELAY
          return nil
        end

        # @type [Discordrb::Attachment]
        attached_file = m.attachments[0]
        if attached_file.size > 1*1024*1024 # 1 MB
          m.delete
          event.send_temp 'Ton fichier est trop lourd !', 2*TEMP_MESSAGE_DELAY
          return nil
        end

        unless ACCEPTED_EXTENSIONS.map(&:downcase).include? File.extname(attached_file.filename).downcase
          m.delete
          event.send_temp "Ce type de fichier n'est pas accepté... J'accepte les #{ACCEPTED_EXTENSIONS.join(', ')} !", 2*TEMP_MESSAGE_DELAY
          return nil
        end

        sound_name = attached_file.filename
        sound_url = attached_file.url

        if matching_sound_with_ext(sound_name.without_ext)
          event << 'Ce son a déjà été ajouté !'
          return nil
        end

        Dir.chdir(SOUNDS_PATH) do
          # This is dangerous as it keeps the whole file in memory instead of streaming it, but since we
          # limite the filesize to 1MB, it's kind of OK ^^'
          File.write(sound_name, open(sound_url).read)
        end

        event.send "#{sound_name} ajouté à la liste !"
        return nil
      end

      command(:sounds, description: LIST_SOUNDS_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        # @type m [Discordrb::Message]
        m = event.message
        sounds = []
        Dir.chdir(SOUNDS_PATH) do
          # Get only the filenames of files with accepted extensions
          sounds = Dir.glob(ACCEPTED_EXTENSIONS.map { |ext| "*#{ext}"}).map(&:without_ext)
        end
        if sounds.empty?
          message = "Aucun son n'est disponible 😢"
        else
          message = "Les sons disponibles sont :\n```\n#{ sounds.sort.join("\n") }\n```"
        end
        m.author.pm(message)
      end

      command(:removesound, description: REMOVE_SOUND_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type sound_name [Array<String>]
      |event, *sound_name|
        matching_file = get_sound_abs_path(sound_name.join(' '))
        if matching_file
          File.delete(matching_file)
          event << "Le son #{sound_name.join(' ')} a été supprimé !"
          return nil
        else
          event << "Aucun son n'a été trouvé... Recommence !"
          return nil
        end
      end

      command([:sound, :s], description: PLAY_SOUND_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type sound_name [Array<String>]
      |event, *sound_name|
        # type [Discordrb::Member]
        user = event.user

        unless event.voice.nil?
          event.send_temp 'Je lis déjà un son, attend ton tour !', SHORT_TEMP_MESSAGE_DELAY
          break
        end

        # @type [Discordrb::Channel]
        voice_chan = user.voice_channel
        if voice_chan.nil?
          event << "Tu n'es pas dans un chan vocal !"
          break
        else
          sound = get_sound_abs_path(sound_name.join(' '))
          if sound
            # @type [Discordrb::Voice::VoiceBot]
            voice_bot = event.bot.voice_connect(voice_chan)
            voice_bot.play_file(sound)
            voice_bot.destroy
            event.message.delete
          else
            event.send_temp "Ce son m'est inconnu...", SHORT_TEMP_MESSAGE_DELAY
            break
          end
        end
        return nil
      end

      command(:stop, description: STOP_SOUND_DESCRIPTION) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        unless event.voice.nil?
          event.voice.destroy
        end
        return nil
      end

      # @param [String] sound_name
      # @return [String] Absolute path to the sound file
      def self.get_sound_abs_path(sound_name)
        # @type [String]
        sound_path = nil
        Dir.chdir(SOUNDS_PATH) do
          filename = matching_sound_with_ext(sound_name)
          sound_path = File.absolute_path(filename) if filename
        end

        return sound_path
      end

      # @param [String] sound_name
      # @return [String] Name of the sound, case-sensitive and with extension
      def self.matching_sound_with_ext(sound_name)
        # @type [String]
        sound_real_name = nil
        Dir.chdir(SOUNDS_PATH) do
          sounds_list = Dir.glob("*{#{ACCEPTED_EXTENSIONS.join(',')}}")
          sound_real_name = sounds_list.find { |filename| filename.without_ext.downcase == sound_name.downcase }
        end
        return sound_real_name
      end
    end
  end
end
