require 'open-uri'

module Bot
  module DiscordCommands
    module ControlCommands
      extend Discordrb::Commands::CommandContainer

      # Change bot username
      # WARNING: This is rate limited by discord, not really sure how much
      command(:setname, help_available: false) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type name [String]
      |event, name|
        if event.user.id == 101005517345800192
          $cr_bot.profile.username = name
        end
        nil
      end

      command(:setavatar, help_available: false) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type text [String]
      |event, text|
        if event.user.id == 101005517345800192
          # @type [Discordrb::Message]
          m = event.message
          if text.nil? || text.empty?
            url = m.attachments[0].url
          else
            url = text
          end
          p url
          open(url) { |pic| event.bot.profile.avatar = pic }
          m.delete
        end
        nil
      end

      command([:exit, :shutdown, :quit], help_available: false) do
      # @type event [Discordrb::Commands::CommandEvent]
      |event|
        if event.user.id == 101005517345800192
          event.message.delete
          event.channel.send_temporary_message 'Goodbye...', TEMP_MESSAGE_DELAY
          sleep 2*TEMP_MESSAGE_DELAY
          $cr_bot.stop
        else
          event.message.delete
          event.channel.send_temporary_message "#{event.user.mention} Accès refusé !", SHORT_TEMP_MESSAGE_DELAY
        end
        return nil
      end

      command(:eval, help_available: false) do
      # @type event [Discordrb::Commands::CommandEvent]
      # @type code [Array<String>]
      |event, *code|
        break unless event.user.id == 101005517345800192
        begin
          event << 'Input:'
          event << "```ruby\n#{code.join(' ')}\n```"
          event << 'Output:'
          event << "```ruby\n#{eval code.join(' ')}\n```"
        rescue => e
          event << "```\n#{e}```"
        end
      end

    end
  end
end
